import React from "react";

export const Carosel = () => {
  return (
    <div className="bg-[#2bb363] flex flex-col gap-3 justify-center items-center text-[#f2faf5] h-[225px]">
      <h1 className="font-semibold text-3xl">
        A joke a day keeps the doctor away
      </h1>
      <h3 className="font-semibold ">
        If you joke wrong way, your teeth have to pay.(Serious)
      </h3>
    </div>
  );
};

export default Carosel;
