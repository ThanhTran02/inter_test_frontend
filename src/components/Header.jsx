import React from "react";

export const Header = () => {
  return (
    <div className="w-4/5 m-auto py-2">
      <div className="flex justify-between items-center bg-transparent">
        <div>
          <a href="#">
            <img src="./img/logo.png" alt="logo" className="object-contain" />
          </a>
        </div>
        <div className=" flex gap-2 items-center">
          <div>
            <p className=" text-[#a1a1a1] text-right">Handicrafted by</p>
            <p className="text-[#535353] text-right font-semibold">Jim HLS</p>
          </div>
          <div className="rounded-full">
            <img
              src="./img/icon.jpg"
              alt="avatar"
              className="rounded-full w-12 h-12"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
