import React from "react";

export const Footed = () => {
  return (
    <div className="w-4/5 m-auto pt-10 pb-5">
      <div className="flex items-center justify-center flex-col gap-4">
        <p className="text-[#9b9b9b] leading-6 text-center ">
          This website is created as part of Hisolutions Program. The materials
          contained on this website are provided for general information only
          and do not constitute any form of advice . HLS assumes no
          responsibility for the accuracy of any particular statement and
          accepts no liability for any loss or damage which may arise from
          reliance on the information contained on this site.
        </p>

        <p className="text-[#767676] font-semibold">Copyright 2021 HLS</p>
      </div>
    </div>
  );
};

export default Footed;
