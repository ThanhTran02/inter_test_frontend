import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Cookies from "js-cookie";
import { toast } from "react-toastify";

import { userActions } from "../store/user/silce";

export const Content = () => {
  const { data, currentStory } = useSelector((state) => state.userManagement);
  const dispatch = useDispatch();

  const handleStory = (vote) => {
    if (currentStory < data.length) {
      dispatch(userActions.setCurrentStory(currentStory + 1));
      Cookies.set(`vote_${currentStory}`, `${vote}`);
    } else {
      dispatch(userActions.setCurrentStory(data.length - 1));
      toast.info("That's all the jokes for today! Come back another day!");
    }
  };

  const handleRepeat = () => {
    dispatch(userActions.setCurrentStory(0));
    const cookies = Object.keys(Cookies.get());
    cookies.forEach((cookieName) => {
      Cookies.remove(cookieName);
    });
  };
  return (
    <div className="w-4/5 m-auto">
      {currentStory < data.length ? (
        <div className="flex items-center justify-center flex-col gap-7  p-10">
          <p className="text-[#737373] leading-7 font-semibold">
            {data[currentStory].content}
          </p>

          <div className="border-t-[1px] border-[#ceccc6] pt-5 mt-5 w-4/5"></div>
          <div className="flex justify-center items-center gap-4 ">
            <button
              className="bg-[#2b7bda] text-white w-[225px] h-[46px] "
              onClick={() => handleStory("dislike")}
            >
              This is Funny!
            </button>
            <button
              className="bg-[#29b363]  text-white  w-[225px] h-[46px] "
              onClick={() => handleStory("like")}
            >
              This is not Funny.
            </button>
          </div>
        </div>
      ) : (
        <div className="flex items-center justify-center flex-col gap-5  p-10">
          <p className="text-[#737373] leading-7 font-semibold">
            That's all the jokes for today! Come back another day!
          </p>
          <div className="border-t-[1px] border-[#ceccc6] pt-5 w-4/5"></div>
          <div className="flex justify-center items-center gap-4 ">
            <button
              className="bg-[#2b7bda] text-white w-[225px] h-[46px] "
              onClick={() => handleRepeat()}
            >
              Repeat
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Content;
