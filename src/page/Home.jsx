import React from "react";

import { Carosel, Content, Footed, Header } from "../components";

export const Home = () => {
  return (
    <div>
      <div>
        <Header />
      </div>
      <div>
        <Carosel />
      </div>
      <div>
        <Content />
      </div>
      <div className="border-t-[2px] border-[#ceccc6] mt-10">
        <Footed />
      </div>
    </div>
  );
};

export default Home;
