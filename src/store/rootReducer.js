import { combineReducers } from "@reduxjs/toolkit";
import { userReducer } from "./user/silce";

export const rootReducer = combineReducers({
  userManagement: userReducer,
});
